import { Container, Row, Col, Tab, Nav } from "react-bootstrap";
import { ProjectCard } from "./ProjectCard";
import projImg1 from "../assets/img/menu21.png";
import projImg2 from "../assets/img/menu22.png";
import projImg3 from "../assets/img/menu23.png";
import projImg4 from "../assets/img/menu24.png";
import projImg5 from "../assets/img/menu11.png";
import projImg6 from "../assets/img/menu12.png";
import projImg7 from "../assets/img/menu13.png";
import projImg8 from "../assets/img/menu14.png";
import projImg9 from "../assets/img/menu32.png";
import projImg10 from "../assets/img/menu33.png";
import projImg11 from "../assets/img/menu34.png";
import colorSharp2 from "../assets/img/color-sharp2.png";
import 'animate.css';
import TrackVisibility from 'react-on-screen';

export const Projects = () => {

  const projects1 = [
    {
      title: "Halaman 1",
      description: "Design & Development",
      imgUrl: projImg1,
    },
    {
      title: "Halaman 2",
      description: "Design & Development",
      imgUrl: projImg2,
    },
    {
      title: "Halaman 3",
      description: "Design & Development",
      imgUrl: projImg3,
    },
    {
      title: "Halaman 4",
      description: "Design & Development",
      imgUrl: projImg4,
    },
  ];

  const projects2 = [
    {
      title: "Halaman 1",
      description: "Design & Development",
      imgUrl: projImg5,
    },
    {
      title: "Halaman 2",
      description: "Design & Development",
      imgUrl: projImg6,
    },
    {
      title: "Halaman 3",
      description: "Design & Development",
      imgUrl: projImg7,
    },
    {
      title: "Halaman 4",
      description: "Design & Development",
      imgUrl: projImg8,
    },
  ];

  const projects3 = [
    {
      title: "Halaman 1",
      description: "Design & Development",
      imgUrl: projImg9,
    },
    {
      title: "Halaman 2",
      description: "Design & Development",
      imgUrl: projImg10,
    },
    {
      title: "Halaman 3",
      description: "Design & Development",
      imgUrl: projImg11,
    },
  ];

  return (
    <section className="project" id="projects">
      <Container>
        <Row>
          <Col size={12}>
            <TrackVisibility>
              {({ isVisible }) =>
              <div className={isVisible ? "animate__animated animate__fadeIn": ""}>
                <h2>Prototype</h2>
                <p>Halaman yang akan digunakan untuk membantu HERO menjadi ideal dan tangguh :</p>
                <Tab.Container id="projects-tabs" defaultActiveKey="first">
                  <Nav variant="pills" className="nav-pills mb-5 justify-content-center align-items-center" id="pills-tab">
                    <Nav.Item>
                      <Nav.Link eventKey="first">Sync Calendar</Nav.Link>
                    </Nav.Item>
                    <Nav.Item>
                      <Nav.Link eventKey="second">Main Menu</Nav.Link>
                    </Nav.Item>
                    <Nav.Item>
                      <Nav.Link eventKey="third">Mobile Based Learning</Nav.Link>
                    </Nav.Item>
                  </Nav>
                  <Tab.Content id="slideInUp" className={isVisible ? "animate__animated animate__slideInUp" : ""}>
                    <Tab.Pane eventKey="first">
                      <Row>
                        {
                          projects1.map((project, index) => {
                            return (
                              <ProjectCard
                                key={index}
                                {...project}
                                />
                            )
                          })
                        }
                      </Row>
                    </Tab.Pane>
                    <Tab.Pane eventKey="second">
                    <Row>
                        {
                          projects2.map((project1, index) => {
                            return (
                              <ProjectCard
                                key={index}
                                {...project1}
                                />
                            )
                          })
                        }
                      </Row>
                    </Tab.Pane>
                    <Tab.Pane eventKey="third">
                    <Row>
                        {
                          projects3.map((project2, index) => {
                            return (
                              <ProjectCard
                                key={index}
                                {...project2}
                                />
                            )
                          })
                        }
                      </Row>
                    </Tab.Pane>
                  </Tab.Content>
                </Tab.Container>
              </div>}
            </TrackVisibility>
          </Col>
        </Row>
      </Container>
      <img className="background-image-right" src={colorSharp2} alt="backgroundimg"></img>
    </section>
  )
}
