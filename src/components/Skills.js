import specific from "../assets/img/specific.png";
import measure from "../assets/img/measure.png";
import real from "../assets/img/real.png";
import relevant from "../assets/img/relevant.png";
import time from "../assets/img/time.png";
import Carousel from 'react-multi-carousel';
import 'react-multi-carousel/lib/styles.css';

export const Skills = () => {
  const responsive = {
    superLargeDesktop: {
      // the naming can be any, depends on you.
      breakpoint: { max: 4000, min: 3000 },
      items: 5
    },
    desktop: {
      breakpoint: { max: 3000, min: 1024 },
      items: 3
    },
    tablet: {
      breakpoint: { max: 1024, min: 464 },
      items: 2
    },
    mobile: {
      breakpoint: { max: 464, min: 0 },
      items: 1
    }
  };

  return (
    <section className="skill" id="skills">
        <div className="container">
            <div className="row">
                <div className="col-12">
                    <div className="skill-bx wow zoomIn">
                        <h2>FEATURES</h2>
                        <p>HERO diberikan berbagai pelatihan online maupun offline untuk dapat mengejar standard kompetensi tersebut dan menjadi HERO yang ideal dan tangguh. Untuk itu, hadir HEROIC dengan menu andalan :</p>
                        <Carousel responsive={responsive} infinite={true} className="owl-carousel owl-theme skill-slider">
                            <div className="item">
                                <img src={specific} alt="Image1" />
                                <h5>Sync Calendar</h5>
                                <p>Menyesuaikan jadwal pembelajaran HERO, dengan sistem yang memblock schedule (menghindari tabrakan schedule)</p>
                            </div>
                            <div className="item">
                                <img src={measure} alt="Image2" />
                                <h5>Push on notification</h5>
                                <p>Mengingatkan HERO terhadap jadwal pembelajaran yang akan datang</p>
                            </div>
                            <div className="item">
                                <img src={real} alt="Image3" />
                                <h5>Mobile Based Learning</h5>
                                <p>Terdiri dari menu :Video, Individual courses dan Pathway per job role</p>
                            </div>
                            <div className="item">
                                <img src={relevant} alt="Image4" />
                                <h5>Reward coins</h5>
                                <p>Pemberian reward setelah melakukan action berupa tonton video ataupun courses yang dapat ditukar dengan voucher ataupun hadiah tunai</p>
                            </div>
                            <div className="item">
                                <img src={time} alt="Image5" />
                                <h5>Consult</h5>
                                <p>Menyediakan adaptive learning yang menyesuaikan pembelajaran HERO, sehingga HERO juga bisa memberikan feedback terkait pembelajaran apa saja yang dibutuhkan, misal menyesuaikan temuan di lapangan.</p>
                            </div>
                            <div className="item">
                                <img src={specific} alt="Image5" />
                                <h5>Forum</h5>
                                <p>Mempertemukan para HERO dalam suatu wadah yang memfasilitasi diskusi dan tukar inspirasi untuk strategi operasional sehari-hari.</p>
                            </div>
                            <div className="item">
                                <img src={measure} alt="Image5" />
                                <h5>Mentorship</h5>
                                <p>Menyediakan wadah mentorship dari berbagai unit bisnis sesuai kebutuhan HERO (tidak hanya dari sesama HERO saja).</p>
                            </div>
                            <div className="item">
                                <img src={real} alt="Image5" />
                                <h5>Assesment</h5>
                                <p>Menilai learning experience yang telah dijalani HERO mengenai pengaruhnya terhadap performa kerja</p>
                            </div>
                        </Carousel>
                    </div>
                </div>
            </div>
        </div>
        {/* <img className="background-image-left" src={colorSharp} alt="Gambar" /> */}
    </section>
  )
}
